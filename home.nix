{ config, pkgs, lib, ... }:

{
  imports =
    [ ./home/sway/sway.nix ./home/git.nix ./home/vscode.nix ./home/themes.nix ];

  nixpkgs.config.allowUnfree = true;

  home = {
    username = "ggwpaiushtha";
    homeDirectory = "/home/ggwpaiushtha";
    packages = with pkgs; [
      glib
      qbittorrent
      lolcat
      neofetch
      duf
      ripgrep
      yt-dlp
      bottom
      wl-clipboard
      discord
      xorg.xeyes
      nur.repos.ilya-fedin.kotatogram-desktop
      libnotify
      xdg-utils
      alsa-utils
      nixfmt
      rnix-lsp
      gnome.file-roller
      obs-studio
      krita
      gimp
      gnupg
    ];
    sessionVariables = {
      # NIXOS_OZONE_WL = "1";
      QT_QPA_PLATFORM = "wayland";
      QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
      XDG_CURRENT_DESKTOP = "sway";
      XDG_SESSION_TYPE = "wayland";
      XDG_SESSION_DESKTOP = "sway";
      GTK_USE_PORTAL = "1";
    };
  };

  systemd.user.services = {
    polkit-agent = {
      Unit = {
        Description =
          "A dbus session bus service that is used to bring up authentication dialogs";
        Documentation = [ "man:polkit(8)" ];
        PartOf = [ "graphical-session.target" ];
      };
      Service = {
        Type = "simple";
        ExecStart =
          "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        RestartSec = 5;
        Restart = "always";
      };
      Install = { WantedBy = [ "sway-session.target" ]; };
    };
    copyq = {
      Unit.PartOf = [ "graphical-session.target" ];
      Install.WantedBy = [ "sway-session.target" ];
      Service = {
        ExecStart = "${pkgs.copyq}/bin/copyq";
      };
    };
  };

  programs = {
    home-manager.enable = true;
    bat.enable = true;
    exa = {
      enable = true;
      enableAliases = true;
    };
    foot = {
      enable = true;
      settings = {
        main = {
          font = "Delugia:size=10";
          dpi-aware = "yes";
        };
      };
    };
    jq.enable = true;
    zsh = {
      enable = true;
      enableCompletion = true;
      dotDir = ".config/zsh";
      initExtraFirst = ''
        autoload -U colors && colors
      '';
      initExtraBeforeCompInit = ''
        # Set required options.
        setopt promptsubst

        # Load required modules.
        autoload -U add-zsh-hook
        autoload -Uz vcs_info

        # Add hook for calling vcs_info before each command.
        add-zsh-hook precmd vcs_info

        # Set vcs_info parameters.
        zstyle ':vcs_info:*' enable git
        zstyle ':vcs_info:*:*' check-for-changes true
        zstyle ':vcs_info:*:*' formats "<%b> "

        PROMPT='%B%F{28}%2~%b %F{148}$vcs_info_msg_0_%F{57}»%f '
      '';
    };
    fzf = {
      enable = true;
      enableZshIntegration = true;
    };
    chromium = {
      enable = true;
      extensions = [
        { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; } # ublock origin
        { id = "hlepfoohegkhhmjieoechaddaejaokhf"; } # refined github
        { id = "cofdbpoegempjloogbagkncekinflcnj"; } # deepl translator
        { id = "gfbliohnnapiefjpjlpjnehglfpaknnc"; } # surfing keys
        { id = "dkaagdgjmgdmbnecmcefdhjekcoceebi"; } # perfect pixel
        { id = "eimadpbcbfnmbkopoojfekhnkhdbieeh"; } # dark reader
        { id = "lfpjkncokllnfokkgpkobnkbkmelfefj"; } # linkclump
        { id = "gaidoampbkcknofoejhnhbhbhhifgdop"; } # censortracker
        { id = "fdpohaocaechififmbbbbbknoalclacl"; } # gofullpage
        { id = "jdbgjlehkajddoapdgpdjmlpdalfnenf"; } # visual bookmarks
      ];
    };
    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };
    zathura.enable = true;
    neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;
    };
    mpv = {
      enable = true;
      config = {
        gpu-context = "wayland";
        hwdec = "vaapi";
        vo = "gpu";
      };
    };
  };

  services = {
    gpg-agent = {
      enable = true;
      pinentryFlavor = "qt";
    };
    udiskie = {
      enable = true;
      tray = "never";
    };
    lorri.enable = true;
  };

  home.stateVersion = "22.11";
}
