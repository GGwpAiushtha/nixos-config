{ config, pkgs, ... }:

let
  userName = "ggwpaiushtha";
  nur-no-pkgs = import (builtins.fetchTarball
    "https://github.com/nix-community/NUR/archive/master.tar.gz") { };

in {
  imports = [
    ./hardware-configuration.nix
    <home-manager/nixos>
    # nur-no-pkgs.repos.ilya-fedin.modules.dbus-broker
  ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot = {
        enable = true;
        consoleMode = "max";
      };
    };
  };

  nix.settings.substituters =
    [ "https://ilya-fedin.cachix.org" "https://nix-community.cachix.org" ];
  nix.settings.trusted-public-keys = [
    "ilya-fedin.cachix.org-1:QveU24a5ePPMh82mAFSxLk1P+w97pRxqe9rh+MJqlag="
    "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
  ];

  documentation.man.generateCaches = true;

  time = {
    timeZone = "Europe/Kirov";
    hardwareClockInLocalTime = true;
  };

  networking = {
    hostName = "Aiushtha-NixOS";
    dhcpcd.enable = false;
    useNetworkd = false;
    useDHCP = false;
  };

  systemd.network = {
    enable = true;
    networks = {
      default = {
        matchConfig.Name = "en*";
        networkConfig.DHCP = "yes";
      };
    };
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = { LC_TIME = "ru_RU.UTF-8"; };
  };
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  zramSwap = {
    enable = true;
    memoryPercent = 50;
  };

  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';

  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [ intel-media-driver vaapiVdpau libvdpau-va-gl ];
    extraPackages32 = with pkgs.pkgsi686Linux; [ vaapiIntel ];
  };

  users = {
    defaultUserShell = pkgs.zsh;
    users.ggwpaiushtha = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      shell = pkgs.zsh;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPb8s1dSWN8p8csDnuP11Me7epIl/i18yT4mqkCSI4HW ggwpaiushtha@gmail.com"
      ];
    };
  };

  home-manager = {
    useGlobalPkgs = true;
    users.ggwpaiushtha = import ./home.nix;
  };

  nixpkgs.config = {
    allowUnfree = true;
    packageOverrides = pkgs: {
      nur = import (builtins.fetchTarball
        "https://github.com/nix-community/NUR/archive/master.tar.gz") {
          inherit pkgs;
          repoOverrides = {
            ilya-fedin = import (builtins.fetchTarball
              "https://github.com/ilya-fedin/nur-repository/archive/master.tar.gz") {
                inherit pkgs;
              };
          };
        };
    };
  };

  security = {
    rtkit.enable = true;
    sudo.extraRules = [{
      users = [ "ggwpaiushtha" ];
      commands = [{
        command = "/run/current-system/sw/bin/psd-overlay-helper";
        options = [ "NOPASSWD" ];
      }];
    }];
    polkit.enable = true;
  };

  environment = {
    systemPackages = with pkgs; [ pciutils profile-sync-daemon ];
    binsh = "${pkgs.dash}/bin/dash";
    # sessionVariables = {
    #   # NIXOS_OZONE_WL = "1";
    #   QT_QPA_PLATFORM = "wayland";
    #   QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
    #   XDG_CURRENT_DESKTOP = "sway";
    #   XDG_SESSION_TYPE = "wayland";
    #   XDG_SESSION_DESKTOP = "sway";
    #   GTK_USE_PORTAL = "1";
    # };
  };

  fonts = {
    fonts = with pkgs; [
      cantarell-fonts
      font-awesome
      freefont_ttf
      noto-fonts-cjk-serif
      noto-fonts-cjk-sans
      symbola
      nur.repos.ilya-fedin.exo2
      (callPackage ./pkgs/delugia { })
      (callPackage ./pkgs/twemoji { })
    ];
    enableDefaultFonts = false;
    fontconfig = {
      defaultFonts = {
        sansSerif = [ "Exo 2" ];
        serif = [ "Exo 2" ];
        monospace = [ "Delugia" ];
        emoji = [ "Twemoji" ];
      };
      hinting.enable = false;
      subpixel = {
        rgba = "none";
        lcdfilter = "none";
      };
    };
  };

  services = {
    openssh = {
      enable = true;
      passwordAuthentication = false;
    };
    greetd = {
      enable = true;
      settings = {
        default_session = {
          # command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd sway";
          # user = "greeter";
          command = "${pkgs.sway}/bin/sway";
          user = "${userName}";
        };
      };
    };
    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
    };
    psd.enable = true;
    fstrim.enable = true;
    gvfs.enable = true;
    # dbus-broker.enable = true;
  };

  programs = {
    zsh.enable = true;
    ssh.startAgent = true;
    dconf.enable = true;
    thunar = {
      enable = true;
      plugins = with pkgs.xfce; [ thunar-archive-plugin thunar-volman ];
    };
  };

  xdg.portal = {
    enable = true;
    wlr = {
      enable = true;
      settings.screencast = {
        output_name = "DP-3";
        max_fps = 60;
        chooser_cmd = "slurp -f %o -or";
        chooser_type = "simple";
      };
    };
    extraPortals = with pkgs; [ xdg-desktop-portal-gtk ];
  };

  # qt5 = {
  #   enable = true;
  #   style = "gtk2";
  #   platformTheme = "gtk2";
  # };

  system.stateVersion = "22.11";
}
