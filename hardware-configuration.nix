{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot = {
    initrd = {
      availableKernelModules =
        [ "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
      kernelModules = [ ];
    };
    kernelModules = [ "kvm-intel" "v4l2loopback" "snd-aloop" ];
    extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback.out ];
    extraModprobeConfig = ''
      options v4l2loopback exclusive_caps=1 card_label="Virtual Camera"
    '';
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/00fbbbf6-fc7d-4876-9882-7e33cb23cae0";
      fsType = "btrfs";
      options = [
        "subvol=@"
        "autodefrag"
        "nobarrier"
        "commit=150"
        "compress=zstd"
        "discard=async"
        "space_cache=v2"
        "noatime"
      ];
    };
    "/home" = {
      device = "/dev/disk/by-uuid/00fbbbf6-fc7d-4876-9882-7e33cb23cae0";
      fsType = "btrfs";
      options = [
        "subvol=@home"
        "autodefrag"
        "nobarrier"
        "commit=150"
        "compress=zstd"
        "discard=async"
        "space_cache=v2"
        "noatime"
      ];
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/D06F-B0CE";
      fsType = "vfat";
      options = [ "noatime" ];
    };
  };

  swapDevices = [ ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;
}
