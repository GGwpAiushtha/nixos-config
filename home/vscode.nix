{pkgs, ...}:

{
  programs.vscode = {
      enable = true;
      extensions = with pkgs.vscode-extensions;
        [ dbaeumer.vscode-eslint eamodio.gitlens bbenoist.nix ]
        ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
          {
            name = "beardedtheme";
            publisher = "BeardedBear";
            version = "6.3.0";
            sha256 = "12x2m5i96zklj8rfxw8qjqahddd83yisfz0b6vjbri4knz3sdbdr";
          }
          {
            name = "nixfmt-vscode";
            publisher = "brettm12345";
            version = "0.0.1";
            sha256 = "07w35c69vk1l6vipnq3qfack36qcszqxn8j3v332bl0w6m02aa7k";
          }
          {
            name = "nix-ide";
            publisher = "jnoortheen";
            version = "0.1.20";
            sha256 = "16mmivdssjky11gmih7zp99d41m09r0ii43n17d4i6xwivagi9a3";
          }
        ];
      userSettings = {

      };
    };
}