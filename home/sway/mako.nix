{ config, pkgs, lib, ... }:

{
  systemd.user.services.mako = {
    Service = { ExecStart = "${pkgs.mako}/bin/mako"; };
    Install = {
      After = [ "sway-session.target" ];
      WantedBy = [ "sway-session.target" ];
    };
  };
  programs.mako = {
    enable = true;
    layer = "overlay";
    defaultTimeout = 10000;
    iconPath = "${pkgs.papirus-icon-theme}/share/icons/Papirus-Dark";
    extraConfig = let
      play = sound:
        "${pkgs.mpv}/bin/mpv ${pkgs.sound-theme-freedesktop}/share/sounds/freedesktop/stereo/${sound}.oga";
    in ''
      on-notify=exec ${play "message"}
      [app-name=yubikey-touch-detector]
      on-notify=exec ${play "service-login"}
      [app-name=command_complete summary~="✘.*"]
      on-notify=exec ${play "dialog-warning"}
      [app-name=command_complete summary~="✓.*"]
      on-notify=exec ${play "bell"}
      [category=osd]
      on-notify=none
    '';
  };
}
