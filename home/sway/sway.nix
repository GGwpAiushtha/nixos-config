{ config, pkgs, lib, ... }:

{
  imports = [ ./mako.nix ./waybar.nix ];

  systemd.user.targets.sway-session = {
    Unit = {
      Description = "sway compositor session";
      Documentation = [ "man:systemd.special(7)" ];
      BindsTo = [ "graphical-session.target" ];
      Wants = [ "graphical-session-pre.target" ];
      After = [ "graphical-session-pre.target" ];
    };
  };

  wayland.windowManager.sway = {
    systemdIntegration = false;
    enable = true;
    extraSessionCommands = ''
      . "${config.home.profileDirectory}/etc/profile.d/hm-session-vars.sh"
    '';
    config = {
      assigns = {
        "2" = [ { app_id = "kotatogramdesktop"; } { class = "discord"; } ];
      };
      bars = [ ];
      modifier = "Mod4";
      terminal = "${pkgs.foot}/bin/foot";
      menu = "${pkgs.rofi-wayland}/bin/rofi -modi drun -show";
      output = {
        "*" = { bg = "/home/ggwpaiushtha/wallpaper/wallpaper.jpg fill"; };
        HDMI-A-3 = { pos = "0 0"; };
        DP-3 = { pos = "1920 0"; };
      };
      workspaceOutputAssign = [
        {
          output = "DP-3";
          workspace = "1";
        }
        {
          output = "DP-3";
          workspace = "2";
        }
        {
          output = "DP-3";
          workspace = "3";
        }
        {
          output = "DP-3";
          workspace = "4";
        }
        {
          output = "DP-3";
          workspace = "5";
        }
        {
          output = "HDMI-A-3";
          workspace = "10";
        }
      ];
      input = {
        "*" = {
          xkb_layout = "us,ru";
          xkb_options = "grp_led:caps,grp:shift_caps_switch";
        };
      };
      bindkeysToCode = true;
      keybindings = let
        modifier = config.wayland.windowManager.sway.config.modifier;
        copyq = "${pkgs.copyq}/bin/copyq";
        grimshot = "${pkgs.sway-contrib.grimshot}/bin/grimshot";
        swappy = "${pkgs.swappy}/bin/swappy";
      in lib.mkOptionDefault {
        "${modifier}+Shift+v" = "exec ${copyq} menu";
        "Shift+Print" = "exec ${grimshot} save area - | ${swappy} -f -";
      };
      seat = { "seat0" = { xcursor_theme = "Numix-Cursor-Light"; }; };
      startup = let
        gtkschema = "org.gnome.desktop.interface";
        easyeffects = "${pkgs.easyeffects}/bin/easyeffects";
      in [
        {
          command = "${easyeffects} --gapplication-service";
          always = true;
        }
        {
          command = "gsettings set $gtkschema gtk-theme 'Qogir-Dark'";
          always = true;
        }
        {
          command = "gsettings set $gtkschema icon-theme 'Papirus-Dark'";
          always = true;
        }
        {
          command =
            "gsettings set $gtkschema cursor-theme 'Numix-Cursor-Light'";
          always = true;
        }
        {
          command = "gsettings set $gtkschema font-name 'Exo 2'";
          always = true;
        }
        { command = "discord"; }
        {
          command = "kotatogram-desktop";
        }
        # { command = "copyq"; }
      ];
      gaps = {
        top = -4;
        right = 12;
        bottom = 20;
        left = 12;
        inner = 4;
      };
      window.commands = [
        {
          criteria = { title = ".*CopyQ"; };
          command = "floating enable";
        }
        {
          criteria = { title = ".*CopyQ"; };
          command = "move position mouse";
        }
        {
          criteria = { app_id = "kotatogramdesktop"; };
          command = "resize set 602 px";
        }
        {
          criteria = { class = "discord"; };
          command = "resize set 1280 px";
        }
        {
          criteria = { window_role = "pop-up"; };
          command = "floating enable";
        }
        {
          criteria = { window_role = "bubble"; };
          command = "floating enable";
        }
        {
          criteria = { window_type = "dialog"; };
          command = "floating enable";
        }
        {
          criteria = { app_id = "pinentry-qt"; };
          command = "floating enable";
        }
      ];
    };
    extraConfig = ''
            exec systemctl --user import-environment DISPLAY WAYLAND_DISPLAY SWAYSOCK && \
                 hash dbus-update-activation-environment 2>/dev/null && \
                 dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK && \
                 systemctl --user start sway-session.target
              default_border pixel 1
    '';
    wrapperFeatures.gtk = true;
  };
}
