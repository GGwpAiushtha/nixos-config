{ config, pkgs, lib, ... }:

{
  programs.waybar = {
    enable = true;
    systemd.enable = true;
    style = "${builtins.readFile "${pkgs.waybar}/etc/xdg/waybar/style.css"}";
    settings = [{
      position = "top";
      height = 22;
      spacing = 4;
      modules-left = [ "sway/workspaces" "sway/mode" ];
      modules-center = [ ];
      modules-right = [
        "pulseaudio"
        "network"
        "cpu"
        "memory"
        "temperature"
        "keyboard-state"
        "sway/language"
        "clock"
        "tray"
      ];
      keyboard-state = {
        numlock = true;
        capslock = true;
        format = "{name} {icon}";
        format-icons = {
          locked = "";
          unlocked = "";
        };
      };
      "sway/mode" = { format = ''<span style="italic">{}</span>''; };
      clock = {
        interval = 1;
        timezone = "Europe/Kirov";
        format = "{:%d %B %Y <b>|</b> %R}";
        tooltip-format = ''
          <big>{:%Y %B}</big>
          <tt><b>{calendar}</b></tt>'';
      };
      cpu = {
        interval = 5;
        format = "{usage}% ";
        tooltip = false;
      };
      memory = {
        interval = 5;
        format = "{}% ";
      };
      temperature = {
        critical-threshold = 80;
        format = "{temperatureC}°C {icon}";
        format-icons = [ "" "" "" ];
      };
      network = {
        format-ethernet = "{ifname} ";
        format-disconnected = "Disconnected";
      };
      pulseaudio = {
        format = "{volume}% {icon} {format_source}";
        format-bluetooth = "{volume}% {icon} {format_source}";
        format-bluetooth-muted = " {icon} {format_source}";
        format-muted = " {format_source}";
        format-source = "{volume}% ";
        format-source-muted = "";
        format-icons = {
          headphone = "";
          hands-free = "";
          headset = "";
          phone = "";
          portable = "";
          car = "";
          default = [ "" "" "" ];
        };
        on-click = "pavucontrol";
      };
    }];
  };
}
