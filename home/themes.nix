{pkgs, ...}:

{
  gtk = {
    enable = true;
    theme = {
      package = pkgs.qogir-theme;
      name = "Qogir-Dark";
    };
    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus-Dark";
    };
    cursorTheme = {
      package = pkgs.numix-cursor-theme;
      name = "Numix-Cursor-Light";
    };
  };

  qt = {
    enable = true;
    platformTheme = "gtk";
    style = { name = "gtk2"; };
  };
}
