{
  programs.git = {
    enable = true;
    userName = "Ivan Bastrakov";
    userEmail = "ggwpaiushtha@gmail.com";
    extraConfig = { init = { defaultBranch = "main"; }; };
    ignores = [ ".envrc" ".direnv" ".shell.nix" "*~" ".#*" "#*#" ];
    signing = {
      key = "2C6D37D46AA1DCDABE8DF34643E2CF4C01B94940";
      signByDefault = true;
    };
  };
}
