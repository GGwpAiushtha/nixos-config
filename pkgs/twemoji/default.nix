{ lib, stdenv, fetchurl, rpmextract }:

let version = "14.0.2";
in stdenv.mkDerivation {
  pname = "ttf-twemoji";
  inherit version;

  src = fetchurl {
    url =
      "https://kojipkgs.fedoraproject.org/packages/twitter-twemoji-fonts/${version}/1.fc37/noarch/twitter-twemoji-fonts-${version}-1.fc37.noarch.rpm";
    sha256 = "d65c31e96cdbefd4d5e463806a620afb3e938f488220eb1f851889a746c0adbc";
  };

  nativeBuildInputs = [ rpmextract ];

  unpackPhase = ''
    rpmextract $src
  '';

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    mkdir -p $out/share/fonts
    cp -rv ./usr/share/fonts/* $out/share/fonts/
  '';

  meta = with lib; {
    homepage = "https://github.com/twitter/twemoji";
    description = "Twitter Emoji for everyone.";
    platforms = platforms.linux;
  };
}
