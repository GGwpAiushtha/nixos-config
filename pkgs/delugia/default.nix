{ lib, fetchzip }:

fetchzip {
  name = "delugia-code-2111.01";

  url =
    "https://github.com/adam7/delugia-code/releases/download/v2111.01/delugia-complete.zip";

  postFetch = ''
    mkdir -p $out/share/fonts/truetype/delugia-code
    unzip -j $downloadedFile \*.ttf -d $out/share/fonts/truetype/delugia-code
  '';

  sha256 = "sha256-AIqnMzh+5XE1Jnc++qk4xWk+csaII+rmoF+eF/QJ6Pw=";

  meta = {
    description = "Cascadia Code + Nerd Fonts, with some small differences.";
    longDescription = ''
      Cascadia Code + Nerd Fonts, with some small differences.
    '';
    homepage = "https://moji.or.jp/ipafont/";
    license = lib.licenses.ipa;
  };
}
